from settings import *

SECRET_KEY = 'skk81urme9c8q-0e^ugq%hg6d1pyft*omv+#b@e9tp2ph-ls@4'
DEBUG = False
ALLOWED_HOSTS = ['proindigo.kg', ]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}