from django.contrib import admin

# Register your models here.
from project.public.models import Image


class ImageAdmin(admin.ModelAdmin):
    list_display = ['name', 'image', ]


admin.site.register(Image, ImageAdmin)
